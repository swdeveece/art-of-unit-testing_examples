﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerUsingFactoryMethodTests
    {
        [Test]
        public void overrideTest()
        {
            FakeExtensionManager stub = new FakeExtensionManager();
            stub.WillBeValid = true;
            TestableLogAnalyzer logan = new TestableLogAnalyzer(stub);

            bool result = logan.IsValidLogFileName("file.ext");

            Assert.True(result);
        }
    }

    internal class TestableLogAnalyzer : LogAnalyzerUsingFactoryMethod

    {
        public IExtensionManager Manager;

        public TestableLogAnalyzer(IExtensionManager mgr)
        {
            Manager = mgr;
        }

        protected override IExtensionManager GetManager()
        {
            return Manager;
        }

        //Exactly the Same as the Previous Fake Extension Manager

        internal class FakeExtensionManager : IExtensionManager
        {
            public bool WillBeValid = false;

            public bool IsValid(string fileName)
            {
                return WillBeValid;
            }
        }
    }
}