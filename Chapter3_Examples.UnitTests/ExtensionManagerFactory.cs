﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3_Examples.UnitTests
{
    public class ExtensionManagerFactory
    {
        private IExtensionManager _customExtensionManager;

        public IExtensionManager Create()
        {
            if (_customExtensionManager != null)
            {
                return _customExtensionManager;
            }
            return new FileExtensionManager();
        }

        public void SetManager(IExtensionManager mgr)
        {
            _customExtensionManager = mgr;
        }
    }
}