﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter3_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerUsingFactoryMethodUpdatedTests
    {
        [Test]
        public void OverrideTestWithoutStub()
        {
            TestableLogAnalyzerUpdated logan = new TestableLogAnalyzerUpdated();

            logan.IsSupported = true;

            bool result = logan.IsValidLogFileName("file.ext");

            Assert.True(result);
        }
    }

    internal class TestableLogAnalyzerUpdated : LogAnalyzerUsingFactoryMethodUpdated
    {
        public bool IsSupported;

        protected override bool IsValid(string fileName)
        {
            return IsSupported;
        }
    }
}