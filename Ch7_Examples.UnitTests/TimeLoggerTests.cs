﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples.UnitTests
{
    [TestFixture]
    public class TimeLoggerTests
    {
        [Test]
        public void SettingSystemTime_Always_ChangeTime()
        {
            SystemTime.Set(new DateTime(2000, 1, 1));
            string output = TimeLogger.CreateMessage("a");
            StringAssert.Contains(@"1/1/2000", output);
        }

        [TearDown]
        public void afterEachTestReset()
        {
            SystemTime.Reset();
        }
    }
}