﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples
{
    public class IISLogStringParser : BaseStringParser
    {
        public IISLogStringParser(string filename) : base(filename)
        {
        }

        public override bool HasCorrectHeader()
        {
            //missing here: real implementation
            return false;
        }

        public override string GetStringVersionFromHeader()
        {
            //missing here: real implementation
            return "";
        }
    }
}