﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples
{
    public class XMLStringParser : BaseStringParser
    {
        public XMLStringParser(string toParse)
            : base(toParse) { }

        public override bool HasCorrectHeader()
        {
            //missing here: logic that parses xml
            return false;
        }

        public override string GetStringVersionFromHeader()
        {
            //missing here: logic that parses xml
            return String.Empty;
        }
    }
}