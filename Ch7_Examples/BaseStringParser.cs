﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch7_Examples
{
    public abstract class BaseStringParser : IStringParser
    {
        private string stringToParse;

        public string StringToParse
        {
            get
            {
                return stringToParse;
            }
        }

        protected BaseStringParser(string filename)
        {
            this.stringToParse = filename;
        }

        public abstract string GetStringVersionFromHeader();

        public abstract bool HasCorrectHeader();
    }
}