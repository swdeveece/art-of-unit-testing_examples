﻿using Ch2_Example;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch2_Examples.UnitTests
{
    [TestFixture]
    public class MemCalculatorTests
    {
        [Test]
        public void Sum_ByDefault_ReturnsZero()
        {
            //Arrange
            MemCalculator calc = MakeCalc();

            //Act
            int lastsum = calc.Sum();

            //Assert
            Assert.AreEqual(lastsum, 0);
        }

        [Test]
        public void Sum_WhenCalled_ChangesSum()
        {
            //Arrange
            var calc = MakeCalc();

            //Act
            calc.Add(5);
            int lastsum = calc.Sum();

            //Assert
            Assert.AreEqual(lastsum, 5);
        }

        private static MemCalculator MakeCalc()
        {
            return new MemCalculator();
        }
    }
}