﻿using Ch4_Examples;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Examples.UnitTests
{
    [TestFixture]
    public class LogAnalyzerTests
    {
        [Test]
        public void Analyze_TooShortFileName_CallsWebService()
        {
            //Arrange
            FakeWebService mockService = new FakeWebService();
            LogAnalyzer log = new LogAnalyzer(mockService);
            string tooShortFilename = "abc.ext";

            //Act
            log.Analyze(tooShortFilename);

            //Assert
            StringAssert.Contains("Filename is too short: " + tooShortFilename, mockService.LastError);
            /*
             * The Assert is performed against the mock object instead of Log Analyzer class
             */
        }
    }
}