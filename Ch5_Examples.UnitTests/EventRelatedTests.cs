﻿using NUnit.Framework;
using System;

using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch5_Examples.UnitTests
{
    [TestFixture]
    public class EventRelatedTests
    {
        //next two tests check for the Listener
        [Test]
        public void ctor_whenViewIsLoaded_CallsViewRender()
        {
            var mockView = Substitute.For<IView>();
            Presenter p = new Presenter(mockView);
            mockView.Loaded += Raise.Event<Action>();
            mockView.Received()
                .Render(Arg.Is<string>(s => s.Contains("Hello World")));
        }

        [Test]
        public void ctor_WhenViewHasError_CallsLogger()
        {
            var stubView = Substitute.For<IView>();
            var mockLogger = Substitute.For<ILogger>();

            Presenter P = new Presenter(stubView, mockLogger);
            //Trigger the Event
            stubView.ErrorOccured += Raise.Event<Action<string>>("fake error");
            //Check the service that was written to
            mockLogger.Received()
                .LogError(Arg.Is<string>(s => s.Contains("fake error")));
        }

        //Testing whether an event was triggered
        [Test]
        public void EventFiringManual()
        {
            bool loadFired = false;
            SomeView view = new SomeView();
            view.Load += delegate
             {
                 loadFired = true;
             };
            view.DoSomethingThatEventuallyFiresThisEvent();
            Assert.IsTrue(loadFired);
        }
    }

    public class Presenter
    {
        private readonly IView _view;
        private readonly ILogger _log;

        public Presenter(IView view)
        {
            _view = view;
            _view.Loaded += OnLoaded;
        }

        public Presenter(IView view, ILogger log)
        {
            _view = view;
            _log = log;
            this._view.Loaded += OnLoaded;
            this._view.ErrorOccured += OnError;
        }

        private void OnError(string text)
        {
            _log.LogError(text);
        }

        private void OnLoaded()
        {
            _view.Render("Hello World");
        }
    }

    public interface IView
    {
        event Action Loaded;

        event Action<string> ErrorOccured;

        void Render(string text);
    }

    public class SomeView
    {
        public event Action Load;

        public SomeView()
        {
            //   Load += DoSomethingThatEventuallFiresThisEvent;
        }

        public void DoSomethingThatEventuallyFiresThisEvent()
        {
            Load();
        }
    }
}